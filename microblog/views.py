# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils import timezone

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect

from models import *

from google.appengine.api import memcache


class DodajForm(forms.Form):
    wpis = forms.CharField(max_length=500)

class LoginForm(forms.Form):
    login = forms.CharField(max_length=100)
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    login = forms.CharField(max_length=100)
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)
    nhaslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class FilterForm(forms.Form):
    by_username = forms.CharField(max_length=100)

def dodaj(request):
    if not memcache.get('problog_dodaj'):
        form = DodajForm() # An unbound form
        strona = render(request, 'microblog/dodawanie.html', {
            'forma': form,
            'zalogowany': request.COOKIES['zalogowany']})
        memcache.add(key="problog_dodaj", value=strona, time=(60*5))
        return strona
    else:
        return memcache.get('problog_dodaj')

def login(request):
    if not memcache.get('problog_login'):
        forma = LoginForm()
        strona = render(request, 'microblog/login.html', {
            'forma': forma,
            'zalogowany': request.COOKIES['zalogowany']})
        memcache.add(key="problog_login", value=strona, time=(60*5))
        return strona
    else:
        return memcache.get('problog_login')

def register(request):
    if not memcache.get('problog_register'):
        forma = RegisterForm()
        strona = render(request, 'microblog/register.html', {
            'forma': forma,
            'zalogowany': request.COOKIES['zalogowany']})
        memcache.add(key="problog_register", value=strona, time=(60*5))
        return strona
    else:
        return memcache.get('problog_register')

def doLogin(request):
    forma = LoginForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            haslo = forma.cleaned_data['haslo']
            ilosc = User.all().filter('login = ', username).filter('haslo = ', haslo).count()
            if ilosc is 1:
                response = HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'login_ok',)))
                response.set_cookie('zalogowany', 'True')
                response.set_cookie('user', username)
                loginuser(login, haslo)
                return response
            else:
                response = HttpResponseRedirect(reverse('microblog.views.komunikat', args=('dsadsa', 'dsadsa',)))
                response.set_cookie('zalogowany', 'False')
                return response
        else:
            return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('dsadsa', 'dsadsa',)))

    else:
        if 'zalogowany' in request.COOKIES:
            zalogowany = request.COOKIES['zalogowany']
            if zalogowany == 'True':
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('dsadsa', 'dsadsa',)))
        else:
            return HttpResponseRedirect(reverse('microblog.views.login'))

def doRegister(request):
    forma = RegisterForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            haslo = forma.cleaned_data['haslo']
            nhaslo = forma.cleaned_data['nhaslo']
            ilosc = User.all().filter('login = ', username).count()
            if ilosc >= 1:
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'user_exists',)))
            else:
                if not haslo == nhaslo:
                    return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'pwd_not_match',)))


                u = User(login=username, haslo=haslo)
                u.put()
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'register_ok',)))

    else:
        return HttpResponseRedirect(reverse('microblog.views.register'))

def komunikat(request, typ, message):
    tresc = ''
    if message == 'login_ok':
        tresc = 'Zalogowano pomyslnie'
    elif message == 'logout_ok':
        tresc = 'Wylogowano pomyslnie'
    elif message == 'not_logged_in':
        tresc = 'Nie jestes zalogowany'
    elif message == 'wpis_ok':
        tresc = 'Twoj wpis dodano pomyslnie'
    elif message == 'user_exists':
        tresc = 'Taki uzytkownik juz istnieje'
    elif message == 'pwd_not_match':
        tresc = 'Podane hasla nie sa identyczne'
    elif message == 'register_ok':
        tresc = 'Rejestracja poprawna. Mozesz sie zalogowac'
    else:
        tresc = 'Cos chyba kombinujesz'

    if typ == 'ok' or typ == 'blad':
        return render(request, 'microblog/komunikat.html',
            {
                'typ': typ,
                'tresc': tresc,
                'zalogowany': request.COOKIES['zalogowany'],
            })
    else:
        return render(request, 'microblog/komunikat.html',
            {
                'typ': 'blad',
                'tresc': tresc,
                'zalogowany': request.COOKIES['zalogowany'],
            })



def index(request):
    zalogowany = ''
    if not 'zalogowany' in request.COOKIES:
        zalogowany = 'False'

        response = render(request, 'microblog/index.html', {
            'zalogowany': zalogowany,
            'wszystkie_wpisy': Wpis.all().order('-data'),
        })
        response.set_cookie('zalogowany', 'False')

        return response
    else:
        return render(request, 'microblog/index.html', {
            'zalogowany': request.COOKIES['zalogowany'],
            'wszystkie_wpisy': Wpis.all().order('-data'),
        })

def logout(request):
    if request.COOKIES['zalogowany'] == 'True':
        response = HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'logout_ok',)))
        response.set_cookie('zalogowany', 'false')
        response.delete_cookie('user')
        return response
    else:
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'not_logged_in',)))


def add(request):
    if request.COOKIES['zalogowany'] == 'True':
        if request.method == 'POST': # If the form has been submitted...
            # ContactForm was defined in the the previous section
            form = DodajForm(request.POST) # A form bound to the POST data
            if form.is_valid():
                user = request.COOKIES['user']
                tresc = form.cleaned_data['wpis']
                data = timezone.now()
                w = Wpis(autor=user, tresc=tresc, data=data)
                w.put()
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'wpis_ok',)))

        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'fdsfds',)))

    else:
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'not_logged_in',)))

def filter(request):
    if not memcache.get('problog_filter'):
        forma = FilterForm()
        strona = render(request, 'microblog/filter.html', {
            'forma': forma,
            'zalogowany': request.COOKIES['zalogowany']})
        memcache.add(key="problog_filter", value=strona, time=(60*5))
        return strona
    else:
        return memcache.get('problog_filter')

def doFilter(request):

    if request.method == 'POST':
        forma = FilterForm(request.POST)

        if forma.is_valid():
            user = forma.cleaned_data['by_username']
            return HttpResponseRedirect(reverse('microblog.views.showUserPosts', args=(user,)))

    return HttpResponseRedirect(reverse('microblog.views.index'))

def showUserPosts(request, user):
    response = render(request, 'microblog/index.html', {
        'zalogowany': request.COOKIES['zalogowany'],
        'wszystkie_wpisy': Wpis.all().filter('autor = ', user).order('-data'),
        })

    if not 'zalogowany' in request.COOKIES:
        response.set_cookie('zalogowany', 'False')

    return response


